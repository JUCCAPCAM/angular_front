'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */

var app = angular.module('main.controller', []);
 app.controller('MainCtrl',['$scope','Task',function($scope,Task) {
 	$scope.tasks = Task.query();
 	$scope.task = Task.get({taskId:1});
  }]);
