var service = angular.module('main.service',['ngResource']);

service.factory('Task', ['$resource','sysvars',
	function($resource,sysvars){
		return $resource(sysvars.back_domain+'/task/:taskId',{taskId:'@id'});
	}]);