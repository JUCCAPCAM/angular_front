'use strict';
/**
 * Main module of the application.
 */
var app = angular.module('sbAdminApp', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'ngRoute',
    'ngResource',
    'ngCookies',
    'smart-table',
    'main.controller',
    'main.service',
  ]);
  app.run(['$http','$cookies',function($http,$cookies){

  }]);

  app.config(['$httpProvider','$stateProvider','$ocLazyLoadProvider','$resourceProvider',
    function ($httpProvider,$stateProvider,$ocLazyLoadProvider,$resourceProvider) {
    //$httpProvider.defaults.xsrfHeaderName='X-CSRFToken';
    //$httpProvider.defaults.xsrfCookieName='csrftoken';
    //$httpProvider.defaults.withCredentials = true;
    /*
    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
     return {
         'request': function (config) {
             config.headers = config.headers || {};
             if ($localStorage.token) {
                 config.headers.Authorization = 'Bearer ' + $localStorage.token;
             }
             return config;
         },
         'responseError': function (response) {
             if (response.status === 401 || response.status === 403) {
                 $location.path('/log');
             }
             return $q.reject(response);
         }
       };
    }]);*/

    $resourceProvider.defaults.stripTrailingSlashes = false;

    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });

    //$urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('app', {
        url:'/app',
        templateUrl: 'views/app/main.html',
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'sbAdminApp',
                    files:[
                    'scripts/directives/header/header.js',
                    'scripts/directives/header/header-notification/header-notification.js',
                    'scripts/directives/sidebar/sidebar.js',
                    'scripts/directives/sidebar/sidebar-search/sidebar-search.js'
                    ]
                }),
                $ocLazyLoad.load(
                {
                   name:'toggle-switch',
                   files:["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                          "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                      ]
                }),
                $ocLazyLoad.load(
                {
                  name:'ngAnimate',
                  files:['bower_components/angular-animate/angular-animate.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:['bower_components/angular-cookies/angular-cookies.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngResource',
                  files:['bower_components/angular-resource/angular-resource.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngSanitize',
                  files:['bower_components/angular-sanitize/angular-sanitize.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngTouch',
                  files:['bower_components/angular-touch/angular-touch.js']
                })
            }
        }
    })
      .state('app.home',{
        url:'/home',
        controller: 'MainCtrl',
        templateUrl:'views/app/home.html',
        
      })
  }]);

app.constant('sysvars',{
  
  back_domain: 'http://localhost:1337',
  auth_back_domain: 'http://localhost:1337',
  
});